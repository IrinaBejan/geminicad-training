#include "CaesarCipher.h"

CaesarCipher::CaesarCipher()
{
}


CaesarCipher::~CaesarCipher()
{
}

std::string CaesarCipher::Encrypt(std::string input, int base) const 
{
	for (int i = 0; i < input.length(); i++)
	{
		if(input[i] >= '0' && input[i] <= 'z')
			input[i] += base;
	}

	return input;
}

std::string CaesarCipher::Decrypt(std::string input, int base) const
{

	for (int i = 0; i < input.length(); i++)
	{
		if (input[i] >= '0' && input[i] <= 127)
			input[i] -= base;
	}

	return input;
}
