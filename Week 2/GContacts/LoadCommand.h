#pragma once

#include "include\ICommand.h"

///
/// \brief Command used to load the ContactsList from disk
///
class LoadCommand : public ICommand, public Singleton<LoadCommand>
{
	friend class Singleton<LoadCommand>;

private:
	LoadCommand();
	virtual ~LoadCommand();

public: /*ICommand*/
	virtual std::string Name() const override;
	virtual std::string Usage() const override;
	virtual CommandStatus ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[]) override;
};

