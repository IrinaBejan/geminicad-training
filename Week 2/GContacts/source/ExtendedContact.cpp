#include "include\ExtendedContact.h"

const char ExtendedContact::exFormatString[] = "%1\t  %2: %3\n";

ExtendedContact::ExtendedContact()
{
  
}

ExtendedContact::~ExtendedContact()
{
  
}

std::string ExtendedContact::SafeGetField(std::string fieldName)
{
  auto result = find(fieldName);
  
  if (result != end())
  {
    return result->second;
  }
  
  return std::string("N/A");
}

void ExtendedContact::SetField(std::string fieldName, std::string fieldValue)
{
  emplace(fieldName, fieldValue);
//  insert_or_assign(fieldName, fieldValue);
}

std::string ExtendedContact::ToString() const
{
  std::string aggregated = Contact::ToString();
  
  for (auto pair : *(map<std::string, std::string>*)this)
  {
    aggregated += pair.first;
    aggregated += pair.second;
  }
  
  return aggregated;
}
