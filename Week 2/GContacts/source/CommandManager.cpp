#include "include\CommandManager.h"

#include <stdio.h>

CommandManager::CommandManager()
{
  m_commands = map<std::string, ICommand*>();
}

CommandManager::~CommandManager()
{
  m_commands.clear();
}

void CommandManager::PrintUsage()
{
  printf("Commands:\n");

  for (auto pair : m_commands)
  {
    printf("%s - %s\n", pair.first.c_str(), pair.second->Usage().c_str());
  }

  printf("\n");
}

bool CommandManager::RegisterCommand(ICommand* target)
{
  try 
  {
//    m_commands.insert(target->Name(), target);
    m_commands.emplace(target->Name(), target);
  }
  catch(const exception&)
  {
    return false;
  }
  
  return true;
}

CommandStatus CommandManager::ProcessArgV(int argumentCount, char* argumentValues[])
{
  int i = 1; //0 is the command line executable used and its path
  
  CommandStatus result;
  
  while (i < argumentCount)
  {
    std::string commandName(argumentValues[i++]); //skip past the command name
    
    auto currentCommand = m_commands.find(commandName);
    
    if (currentCommand != m_commands.end())
    {
      result.Combine(currentCommand->second->ExecuteWithArguments(i, argumentCount, argumentValues));
    }
    else
    {
      result.Combine(CommandStatus(CommandStatus::failure, "invalid command"));
    }
  }
  
  return result;
}
