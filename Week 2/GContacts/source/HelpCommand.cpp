#include "include\HelpCommand.h"

#include "stdio.h"

#include "include\Contact.h"
#include "include\ExtendedContact.h"
#include "include\ContactsList.h"

#include "include\CommandManager.h"

HelpCommand::HelpCommand()
{
  CommandManager::Instance().RegisterCommand(this);
}

HelpCommand::~HelpCommand()
{
  
}

std::string HelpCommand::Name() const
{
  return std::string("help");
}

std::string HelpCommand::Usage() const
{
  return std::string("GContacts help");
}

CommandStatus HelpCommand::ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[])
{
  argumentCount;
  argumentValues;
  currentArgument;

  CommandManager::Instance().PrintUsage();

  return CommandStatus(CommandStatus::eCommandStatus::success, "");  
}

