#include "include\AddCommand.h"

#include "stdio.h"

#include "include\Contact.h"
#include "include\ExtendedContact.h"
#include "include\ContactsList.h"

#include "include\CommandManager.h"

AddCommand::AddCommand()
{
  CommandManager::Instance().RegisterCommand(this);
}

AddCommand::~AddCommand()
{

}

std::string AddCommand::Name() const
{
  return std::string("add");
}

std::string AddCommand::Usage() const
{
  return std::string("GContacts add fName=John lName=Doe number=555-90210");
}

CommandStatus AddCommand::ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[])
{
  char label[25], value[25];
  ExtendedContact* target = new ExtendedContact();
  
  printf("Processing ADD request.\n");
  
  for (int i = currentArgument; i < argumentCount; i ++)
  {
    //safe, bounds-checked read of the argument strings
    sscanf_s(argumentValues[i], "%24[^=]=%24s", label, (unsigned)_countof(label), value, (unsigned)_countof(value));
    
    if (!strcmp (label, "fName"))
    {
      printf("\tGot first name '%s'.\n", value);
      target->SetFirstName(value);
      currentArgument++; //consume input
    }
    else if (!strcmp (label, "lName"))
    {
      printf("\tGot last name '%s'.\n", value);
      target->SetLastName(value);
      currentArgument++; //consume input
    }
    else if (!strcmp (label, "number"))
    {      
      printf("\tGot phone number '%s'.\n", value);
      target->SetPhoneNumber(value);
      currentArgument++; //consume input
    }
    else if (label[0] == '#')
    {
      printf("\tGot field '%s' : '%s'.\n", label+1, value);
      target->SetField(label+1, value);
      currentArgument++;
    }
    else
    {
      break;
    }
  }
  
  ContactsList::Instance().AddContact(target);
  printf("Contact added.\n\n");
  
  return CommandStatus(CommandStatus::eCommandStatus::success, "");  
}

