#include "include\ContactsList.h"

ContactsList::ContactsList()
{
  contacts = vector<Contact*>();
}

ContactsList::~ContactsList()
{
  for (auto contact : contacts)
    delete contact;

  contacts.clear();
}

void ContactsList::AddContact(Contact* target)
{
  contacts.push_back(target);
}

void ContactsList::RemoveLastContact()
{
  delete contacts.back();
  contacts.pop_back();
}

vector<Contact*>::const_iterator ContactsList::begin()
{
  return contacts.cbegin();
}

vector<Contact*>::const_iterator ContactsList::end()
{
  return contacts.cend();
}
