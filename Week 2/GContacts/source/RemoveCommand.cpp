#include "include\RemoveCommand.h"

#include "stdio.h"

#include "include\Contact.h"
#include "include\ExtendedContact.h"
#include "include\ContactsList.h"

#include "include\CommandManager.h"

RemoveCommand::RemoveCommand()
{
  CommandManager::Instance().RegisterCommand(this);
}

RemoveCommand::~RemoveCommand()
{

}

std::string RemoveCommand::Name() const
{
  return std::string("removelast");
}

std::string RemoveCommand::Usage() const
{
  return std::string("GContacts removelast");
}

CommandStatus RemoveCommand::ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[])
{
  argumentCount;
  argumentValues;
  currentArgument;

  printf("Processing Remove last contact request.\n");

  ContactsList::Instance().RemoveLastContact();
  printf("Last contact removed.\n\n");

  return CommandStatus(CommandStatus::eCommandStatus::success, "");
}
