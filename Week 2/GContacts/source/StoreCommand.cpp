#include "include\StoreCommand.h"

#include "stdio.h"
#include "memory"

#include "include\Contact.h"
#include "include\ExtendedContact.h"
#include "include\ContactsList.h"

#include "include\CommandManager.h"

#include "ICipher.h"
#include "CaesarCipher.h"
#define SHIFT 5
StoreCommand::StoreCommand()
{
  CommandManager::Instance().RegisterCommand(this);
}

StoreCommand::~StoreCommand()
{
  
}

std::string StoreCommand::Name() const
{
  return std::string("store");
}

std::string StoreCommand::Usage() const
{
  return std::string("GContacts store path=filename.txt type=ciphername");
}

CommandStatus StoreCommand::ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[])
{
  argumentCount;
  argumentValues;

  printf("Processing store to file request.\n");

  char label[25], value[25];
  char path[25], type[25];
  for (int i = currentArgument; i < argumentCount; i++)
  {
	  sscanf_s(argumentValues[currentArgument], "%24[^=]=%24s", label, (unsigned)_countof(label), value, (unsigned)_countof(value));

	  if (!strcmp(label, "path"))
	  {
		  printf("\tGot path name '%s'.\n", value);
		  strcpy(path, value);
		  currentArgument++;

	  }

	  else if (!strcmp(label, "type"))
	  {
		  printf("\tGot type name '%s'.\n", value);
		  strcpy(type, value);
		  currentArgument++; 
	  }
	  else
	  {
		  break;
	  }
  }

  if (FILE* fout = fopen(value, "w"))
  {
	  ICipher* cipher;
	  if (!strcmp(type, "CaesarCipher"))
	  {
		  std::unique_ptr<ICipher> cipher(new CaesarCipher);
	  }
	  if (!strcmp(type, "BaconCipher"))
	  {
		  //TODO
	  }

	  for (auto it = ContactsList::Instance().begin(); it != ContactsList::Instance().end(); it++)
	  {
		  Contact* temp;
		  temp = *it;

		  fprintf(fout, "%s", cipher->Encrypt((*temp).ToString(),SHIFT).c_str());
	  }

	  return CommandStatus(CommandStatus::eCommandStatus::success, "");
  }
}

