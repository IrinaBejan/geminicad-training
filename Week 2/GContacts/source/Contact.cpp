#include "include\Contact.h"

const char Contact::formatString[] = "%1 , %2 - %3\n";

Contact::Contact()
{

}

Contact::~Contact()
{
	
}

std::string Contact::ToString() const
{
  std::string str;
  str += lastName;
  str += " , ";
  str += firstName;
  str += " - ";
  str += phoneNumber;
  str += "\n";
  return str;
}
