#include "LoadCommand.h"

#include "stdio.h"
#include "memory"

#include "include\Contact.h"
#include "include\ExtendedContact.h"
#include "include\ContactsList.h"

#include "include\CommandManager.h"

#include "ICipher.h"
#include "CaesarCipher.h"
#define SHIFT 5
LoadCommand::LoadCommand()
{
	CommandManager::Instance().RegisterCommand(this);
}

LoadCommand::~LoadCommand()
{

}

std::string LoadCommand::Name() const
{
	return std::string("load");
}

std::string LoadCommand::Usage() const
{
	return std::string("GContacts load path=filename.txt type=ciphername");
}

CommandStatus LoadCommand::ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[])
{
	argumentCount;
	argumentValues;

	printf("Processing load to file request.\n");

	char label[25], value[25];
	char path[25], type[25];
	for (int i = currentArgument; i < argumentCount; i++)
	{
		sscanf_s(argumentValues[currentArgument], "%24[^=]=%24s", label, (unsigned)_countof(label), value, (unsigned)_countof(value));

		if (!strcmp(label, "path"))
		{
			printf("\tGot path name '%s'.\n", value);
			strcpy(path, value);
		}

		if (!strcmp(label, "type"))
		{
			printf("\tGot type name '%s'.\n", value);
			strcpy(type, value);
		}
	}

	if (FILE* fin = fopen(path, "r"))
	{
		ICipher* cipher;
		if (!strcmp(type, "CaesarCipher"))
		{
			std::unique_ptr<ICipher> cipher(new CaesarCipher);
		}
		if (!strcmp(type, "BaconCipher"))
		{
			//TODO
		}

		char fName[25], lName[25], number[25]; 
		while(fscanf(fin, "%s , %s - %s\n",&fName, &lName, &number)!=EOF)
		{
			Contact* target = new Contact();
			target->SetFirstName(cipher->Decrypt(fName, SHIFT));
			target->SetLastName(cipher->Decrypt(lName, SHIFT));
			target->SetPhoneNumber(cipher->Decrypt(number, SHIFT));
			ContactsList::Instance().AddContact(target);
		}

		return CommandStatus(CommandStatus::eCommandStatus::success, "");
	}
	else
	{
		return CommandStatus(CommandStatus::eCommandStatus::failure, "The path file specified for the load command doesn't exist.");
	}

}

