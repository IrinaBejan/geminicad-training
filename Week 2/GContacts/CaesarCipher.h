#include <string>
#include "ICipher.h"

class CaesarCipher : public ICipher
{
public:
	CaesarCipher();
	virtual ~CaesarCipher();
	virtual std::string Encrypt(std::string input, int base) const override;
	virtual std::string Decrypt(std::string input, int base)const override;
};
