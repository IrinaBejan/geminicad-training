#pragma once

#include "include\ICommand.h"

///
/// \brief Command used to remove last contact from ContactsList
///
class RemoveCommand : public ICommand, public Singleton<RemoveCommand>
{
  friend class Singleton<RemoveCommand>;

private:
  RemoveCommand();
  virtual ~RemoveCommand();

public: /*ICommand*/
  virtual std::string Name() const override;
  virtual std::string Usage() const override;
  virtual CommandStatus ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[]) override;
};
