#pragma once

#include "string"

///
/// \brief Interface allowing an object to specify its own string representation.
///
class IStringRepresentable
{
public:
  virtual std::string ToString() const = 0;
};
