#pragma once

#include "include\ICommand.h"

///
/// \brief Command used display help
///
class HelpCommand : public ICommand, public Singleton<HelpCommand>
{
  friend class Singleton<HelpCommand>;

private:
  HelpCommand();
  virtual ~HelpCommand();

public: /*ICommand*/
  virtual std::string Name() const override;
  virtual std::string Usage() const override;
  virtual CommandStatus ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[]) override;
};

