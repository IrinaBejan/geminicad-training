#pragma once

#include "include\ICommand.h"

///
/// \brief Command used to store contacts from ContactsList to disk
///
class StoreCommand : public ICommand, public Singleton<StoreCommand>
{
  friend class Singleton<StoreCommand>;

private:
  StoreCommand();
  virtual ~StoreCommand();

public: /*ICommand*/
  virtual std::string Name() const override;
  virtual std::string Usage() const override;
  virtual CommandStatus ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[]) override;
};

