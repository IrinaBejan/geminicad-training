#pragma once

///
/// \brief Singleton template.
///        usage:
///        T must have a private c-tor
///        T must add Singleton<T> as a friend (for c-tor access)
///        T can inherit Singleton<T> for easy access to single instance
///        note:
///        m_SingleInstance is constructed once at the first call of T::Instance(),
///        also T-s d-tor is guaranteed to be called
///
template<typename T>
class Singleton
{
public:
  ///
  /// \brief
  /// \return Returns the single instance of T
  ///
  static T& Instance()
  {
    static T m_SingleInstance;
    return m_SingleInstance;
  }
};
