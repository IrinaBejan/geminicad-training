#pragma once

#include <vector>
#include "include\Contact.h"
#include "include\Singleton.h"

using namespace std;

///
/// \brief Used to store and manage contact entries.
///
class ContactsList : public Singleton<ContactsList>
{
  friend class Singleton<ContactsList>;

private:
  ContactsList();
  ~ContactsList();

public:
  void AddContact(Contact* target);
  void RemoveLastContact();
  
  vector<Contact*>::const_iterator begin();
  vector<Contact*>::const_iterator end();
  
private: /*fields*/
  vector<Contact*> contacts;
};
