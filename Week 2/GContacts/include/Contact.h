#pragma once

#include "IStringRepresentable.h"

///
/// \brief Represents a single Contact entry.
///
class Contact : public IStringRepresentable
{
public:
  Contact();
  virtual ~Contact();

public: /*IStringRepresentable*/
  virtual std::string ToString() const override;
  
protected: /*fields*/
  std::string firstName;
  std::string lastName;
  std::string phoneNumber;
  
  static const char formatString[];
  
public: /*accessors*/
  std::string FirstName() { return firstName; }
  std::string LastName() { return lastName; }
  std::string PhoneNumber() { return phoneNumber; }
  
  void SetFirstName(std::string _firstName) { firstName = _firstName; }
  void SetLastName(std::string _lastName) { lastName = _lastName; }
  void SetPhoneNumber(std::string _phoneNumber) { phoneNumber = _phoneNumber; }
};
