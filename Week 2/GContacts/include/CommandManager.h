#pragma once

#include <map>

#include "include\ICommand.h"
#include "include\Singleton.h"

using namespace std;

struct CommandStatus;

///
/// \brief Manages all available commands. 
/// Any defined command should use RegisterCommand to add itself to the CommandManager.
///
class CommandManager final : public Singleton<CommandManager> //final - can not be derived from
{
  friend class Singleton<CommandManager>;

private:
  CommandManager();
  ~CommandManager();

public:
  ///
  /// \brief Shows the Usage() suggestions for all registered ICommand handlers.
  ///
  void PrintUsage();
  
  ///
  /// \brief Registers a given command as handler for its name.
  /// \param target the target ICommand object.
  /// \return Returns true if successfully added, false otherwise.
  ///
  bool RegisterCommand(ICommand* target);
  
  ///
  /// \brief Given a list of arguments and their count processes them and dispatches 
  /// their resolution to the respective registered ICommand handlers.
  /// \param argumentCount the number of arguments
  /// \param argumentValues the values for the arguments to be processed
  /// \return Returns a CommandStatus object detailing the result of the operation
  /// and any errors that occured.
  ///  
  CommandStatus ProcessArgV(int argumentCount, char* argumentValues[]);
  
protected: /*fields*/

  map<std::string, ICommand*> m_commands;
};
