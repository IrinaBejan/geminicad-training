#include <stdio.h>

#include "include\CommandManager.h"
#include "include\AddCommand.h"
#include "include\ListCommand.h"
#include "include\HelpCommand.h"
#include "include\StoreCommand.h"
#include "include\RemoveCommand.h"
#include "LoadCommand.h"


const char usageString[] = "usage: GContacts command [arguments].\n"
                           "Type 'GContacts help command_name' "
                           "to find out more about a specific command.\n";

void PrintUsage()
{
  printf (usageString);

  CommandManager::Instance().PrintUsage();
}

int main(int argc, char *argv[])
{
#ifdef _DEBUG

  if (argc == 1)
  {
	  char *targv[] = {(char*)"program name",
		  (char*)"help",
		  (char*)"add", (char*)"fName=A", (char*)"lName=a", (char*)"number=0",
		  (char*)"add", (char*)"fName=B", (char*)"lName=b", (char*)"number=1",
		  (char*)"add", (char*)"fName=C", (char*)"lName=c", (char*)"number=2",
		  (char*)"list",
		  (char*)"removelast",
		  (char*)"list",
		  (char*)"add", (char*)"fName=D", (char*)"lName=d", (char*)"number=3",
		  (char*)"list",
		  (char*)"store", (char*)"path=list.txt", (char*)"type=CaesarCipher",
nullptr};
	/*char *targv[] = { (char*)"program name",
		(char*)"help",
		(char*)"list",
		(char*)"load", (char*)"path=list.txt", (char*)"cipherType=Caesar",
		(char*)"list",
		nullptr };*/
    argv = targv;
    argc = (int)(sizeof(targv) / sizeof(char*)) - 1;
  }

#endif//_DEBUG

  HelpCommand::Instance();
  ListCommand::Instance();
  AddCommand::Instance();
  StoreCommand::Instance();
  RemoveCommand::Instance();
  LoadCommand::Instance();
  if (argc < 2)
  {
    PrintUsage();
  }

  CommandStatus result = CommandManager::Instance().ProcessArgV(argc, argv);
  
  if (result.result == CommandStatus::error)
  {
    perror(result.message.c_str());
  }
  else if (result.result == CommandStatus::failure)
  {
    printf("Failed: %s\n", result.message.c_str());
  }
  
  return result.result == CommandStatus::success;
}
