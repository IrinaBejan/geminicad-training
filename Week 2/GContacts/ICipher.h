#pragma once
#include <string>

class ICipher
{
public:
	virtual std::string Encrypt(std::string input, int base) const = 0;
	virtual std::string Decrypt(std::string input, int base) const = 0;
};