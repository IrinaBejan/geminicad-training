#include <Windows.h>
#include <WinUser.h>

#include <stdio.h>
#include <string>
#include <iostream>

typedef void(*PPrintMessage)(void);


static HINSTANCE hinstDLL;
static HHOOK hHook;
static PPrintMessage printMessage = nullptr;

void LoadAndAttachHookDll()
{
	HOOKPROC hookProc;

	hinstDLL = LoadLibrary(TEXT("KeyboardHook.dll"));
	hookProc = (HOOKPROC)GetProcAddress(hinstDLL, "LowLevelKeyboardHook");
	printMessage = (PPrintMessage)GetProcAddress(hinstDLL, "PrintMessage");

	hHook = SetWindowsHookEx(WH_KEYBOARD_LL, hookProc, 0, 0); //observer la mesajele low lvl din os
}

void DetachAndUnloadHookDll()
{
	UnhookWindowsHookEx(hHook);
	//FreeLibrary(hinstDLL);
}


//Returns the last Win32 error, in string format. Returns an empty string if there is no error.
std::string GetLastErrorAsString()
{
	//Get the error message, if any.
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0)
		return std::string(); //No error message has been recorded

	LPSTR messageBuffer = nullptr;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	std::string message(messageBuffer, size);

	//Free the buffer.
	LocalFree(messageBuffer);

	return message;
}

typedef struct TargetFile
{
	LPCWSTR directory;
} TTargetFile, *PTargetFile;

DWORD WINAPI FileWatcher(LPVOID target)
{
	HANDLE FileChangeHandle;
	DWORD WaitStatus;
	
	do
	{
		FileChangeHandle = FindFirstChangeNotification(((PTargetFile)target)->directory, false, FILE_NOTIFY_CHANGE_LAST_WRITE);

		if (FileChangeHandle == INVALID_HANDLE_VALUE)
		{
			printf("\n ERROR: FindFirstChangeNotification function failed.\n");
			printf(GetLastErrorAsString().c_str());

			system("pause");

			ExitProcess(GetLastError());
		}

		WaitStatus = WaitForSingleObject(FileChangeHandle, INFINITE);

		DetachAndUnloadHookDll();

		LoadAndAttachHookDll();
	} while (true);

	return 0;
}

int main(int argc, char* argv[])
{
	LoadAndAttachHookDll();

	if (printMessage != nullptr)
		printMessage();

	PTargetFile targetFile;
	DWORD watcherThreadId;
	HANDLE watcherThreadHandle;

	targetFile = (PTargetFile)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
		sizeof(TTargetFile));

	targetFile->directory = TEXT("C:\\Users\\irina\\Desktop\\GeminiCad\\fiipractic17\\Week 5\\DllHotReload\\build\\x64\\Debug");

	watcherThreadHandle = CreateThread(NULL, 0, FileWatcher, targetFile, NULL, &watcherThreadId);
	
	MSG msg;

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	WaitForSingleObject(watcherThreadHandle, INFINITE);

	CloseHandle(watcherThreadHandle);

	if (targetFile != nullptr)
	{
		HeapFree(GetProcessHeap(), 0, targetFile);
		targetFile = nullptr;
	}

	DetachAndUnloadHookDll();

	return 0;
}