#pragma once

#include "KeyboardHook_API.h"

#include <Windows.h>

extern "C"
{
	keyboard_hook_dll_api LRESULT CALLBACK LowLevelKeyboardHook
	(
		_In_ int    nCode,
		_In_ WPARAM wParam,
		_In_ LPARAM lParam
	);

	keyboard_hook_dll_api void PrintMessage(void);
}