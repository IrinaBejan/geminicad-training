#pragma once

#ifdef KEYBOARD_HOOK_API_EXPORT
#define keyboard_hook_dll_api __declspec( dllexport )
#else
#define keyboard_hook_dll_api __declspec( dllimport )
#endif
