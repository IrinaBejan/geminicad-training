#include "KeyboardHook.h"

#include <stdio.h>

LRESULT CALLBACK LowLevelKeyboardHook (_In_ int nCode, _In_ WPARAM wParam, _In_ LPARAM lParam)
{
	if (nCode < 0)
	{
		return CallNextHookEx(0, nCode, wParam, lParam);
	}

	printf("You pressed a key!\n");
	return CallNextHookEx(0, nCode, wParam, lParam);

	return 0;
}

void PrintMessage(void)
{
	printf("Wellcome!\n");
}