#include "stdafx.h"
#include "FileParser.h"
#include "Shlwapi.h"
#include <fstream>
//!
//!........................................Constructor................................................
//!
SInfoFound::SInfoFound()
{
	Reset();
}
//!
//!........................................Destructor.................................................
//!
SInfoFound::~SInfoFound()
{
}
//!
//!........................................Reset......................................................
//!
void SInfoFound::Reset()
{
	m_fileOffset = 0;
	m_prefix     = _T("");
	m_sufix      = _T("");
	m_fileName   = _T("");
}
//!
//!........................................Copy Constructor...........................................
//!
SInfoFound::SInfoFound(const SInfoFound& source)
{
	(*this) = source;
}
//!
//!........................................Assign operator............................................
//!
SInfoFound& SInfoFound::operator = (const SInfoFound& source)
{
	if (*this == source)
		return (*this);
	
	m_fileOffset = source.m_fileOffset;
	m_prefix     = source.m_prefix;
	m_sufix      = source.m_sufix;
	m_fileName   = source.m_fileName;

	return (*this);
}
//!
//!........................................Equality operator..........................................
//!
bool SInfoFound::operator == (const SInfoFound& source)
{ 
	bool ok = true;

	ok = ok && (m_fileOffset == source.m_fileOffset);
	ok = ok && (m_prefix     == source.m_prefix);
	ok = ok && (m_sufix      == source.m_sufix);
	ok = ok && (m_fileName   == source.m_fileName);

	return ok;
}
//!
//!........................................Constructor................................................
//!
CFileParser::CFileParser(const std::string& filePath, const std::string& searchStr) : 
						m_filePath(filePath), m_searchStr(searchStr)
{
}
//!
//!........................................Destructor.................................................
//!
CFileParser::~CFileParser()
{
}
//!
//!........................................Parser.....................................................
//!
bool CFileParser::ParseFile(PARSE_RESULT& result)
{
	bool ok = true;
	int offset = 0;
	std::ifstream file;
	std::string line;

	result.clear();
	std::string fileP = m_filePath;
	TCHAR* str = const_cast<TCHAR*>(fileP.data());
	PathStripPath(str);
	std::string fileName = std::string(str);

	file.open(m_filePath.c_str());
	if (file.is_open()) 
	{
		while (!file.eof()) 
		{
			getline(file, line);
			int pos = 0;
			while ((pos = line.find(m_searchStr, pos)) != std::string::npos)
			{
				SInfoFound found;
				found.m_fileOffset = pos + offset;
				GetPrefixAndSuffix(line, pos, m_searchStr.length(), found.m_prefix, found.m_sufix);
				found.m_fileName = fileName;
				result.push_back(found);

				pos += m_searchStr.length();
			}
			offset += line.length();
		}
		file.close();
	}
	else 
		_tprintf(_T("  %s can not be opened!!\n"), m_filePath);

	return ok;
}
//!
//!........................................GetPrefixAndSuffix.........................................
//! In: line from the file
//! In: position where the search string is starting from in the line 
//! In: length of the search string
//! Out: prefix = three characters in front of the search string
//! Out: suffix = three characters after the search string
//!
void CFileParser::GetPrefixAndSuffix(const std::string& line, int pos, int textLen, std::string& prefix, std::string& suffix)
{
	// Get prefix
	prefix.clear();
	for (int i = pos - 3; i >= 0 && i < pos; i++)
	{
		char c = line.at(i);
		if (c == '\t')
			prefix += _T("\t");
		else if (c == '\n')
			prefix += _T("\n");
		else
			prefix.push_back(c);
	}
	suffix.clear();
	int count = -1;
	for (size_t i = pos + textLen; ++count < 3 && i < line.length(); i++)
	{
		char c = line.at(i);
		if (c == '\t')
			suffix += _T("\t");
		else if (c == '\n')
			suffix += _T("\n");
		else
			suffix.push_back(c);
	}
}