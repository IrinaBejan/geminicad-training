#include "stdafx.h"
#include "ThreadFileParser.h"
#include "FileParser.h"
//!
//!........................................Constructor................................................
//! In : files to be parsed
//! In : string to fing in the files 
//! In : thread numbers to be used
//!
CThreadFileParser::CThreadFileParser(const FILE_LIST& files, const std::string& searchStr, int threadNum):
									 m_searchStr(searchStr), m_threadNum(threadNum)
{
	for (auto it : files)
		m_files.push_back(std::make_pair(it, false));
}
//!
//!........................................Destructor.................................................
//!
CThreadFileParser::~CThreadFileParser()
{
	size_t i = 0;
	for (i = 0; i < m_params.size(); i++)
		delete m_params[i];

	for (i = 0; i < m_workThreads.size(); i++)
		CloseHandle(m_workThreads[i]);
	CloseHandle(m_mutex);
}
//!
//!........................................InitInternal.................................................
//! Used to init internal data.
//!
bool CThreadFileParser::InitInternal()
{
	bool ok; 
	m_mutex = CreateMutex(NULL, FALSE, NULL);
	
	ok = m_mutex != NULL;
	
	if (ok == false)
	{
		_tprintf("CreateMutex error: %d\n", GetLastError());
		return ok;
	}
	m_workThreads.resize(m_threadNum);
	m_params.resize(m_threadNum);

	for (int i = 0; ok && i < m_threadNum; i++)
	{
		DWORD ThreadID;
		ThreadParam* param = new ThreadParam();
		param->m_filePaths      = &m_files;
		param->m_stringToSearch = m_searchStr;
		param->m_mutexHandler   = m_mutex;
		
		m_params[i] = param;
		m_workThreads[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ParseFile, (void*)param, 0, &ThreadID);
		ok = ok & (m_workThreads[i] != NULL);

		if (ok == false)
		{
			_tprintf("CreateThread error: %d\n", GetLastError());
			return ok;
		}
	}

	WaitForMultipleObjects(m_threadNum, &m_workThreads[0], TRUE, INFINITE);

	return ok;
}
//!
//!........................................ParseFile.....................................................
//! Used by each thread to parse files
//!
void CThreadFileParser::ParseFile(void* lpParam)
{
	DWORD  dwWaitResult;

	ThreadParam* param = (ThreadParam*)lpParam;

	dwWaitResult = WaitForSingleObject(param->m_mutexHandler, INFINITE);

	if (dwWaitResult == WAIT_OBJECT_0)
	{
		for (size_t i = 0; i < param->m_filePaths->size(); i++)
			if (param->m_filePaths->at(i).second == false)
			{
				CFileParser::PARSE_RESULT result;
				CFileParser parser(param->m_filePaths->at(i).first, param->m_stringToSearch);
				param->m_filePaths->at(i).second = true;
				ReleaseMutex(param->m_mutexHandler);

				parser.ParseFile(result);

				for (auto it : result)
					_tprintf(_T("%s(%d) : %s....%s \n"), it.m_fileName.c_str(), it.m_fileOffset, it.m_prefix.c_str(), it.m_sufix.c_str());
			}
	}
}