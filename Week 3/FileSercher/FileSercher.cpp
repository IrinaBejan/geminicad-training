// FileSercher.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <thread>
#include <ppl.h>
#include <concurrent_vector.h>
#include <chrono>
#include "ComandLineParser.h"
#include "FileParser.h"
#include "ThreadFileParser.h"

using namespace concurrency;

void ParseFileAndOutput(const std::string& filePath, const std::string& searchStr)
{
	CFileParser::PARSE_RESULT result;
	CFileParser parser(filePath, searchStr);
	parser.ParseFile(result);
	for (auto it : result)
		_tprintf(_T("%s(%d) : %s....%s \n"), it.m_fileName.c_str(), it.m_fileOffset, it.m_prefix.c_str(), it.m_sufix.c_str());
}

template <class Function>
float time_call(Function&& f)
{
	typedef std::chrono::high_resolution_clock clock;
	typedef std::chrono::duration<float, std::milli> duration;

	static clock::time_point start = clock::now();
	f();
	duration elapsed = clock::now() - start;

	return elapsed.count();
}

int _tmain(int argc, _TCHAR* argv[])
{
	bool ok;
	float elapsed;
	
	CComandLineParser::FILE_PATHS_VEC files;
	CComandLineParser cmdParser(argc, argv);
	
	ok = cmdParser.GetFilesPaths(files);

	if (ok == false)
		return 1;

	// serial for_each
	_tprintf(_T("START Serial search\n"));
	elapsed = time_call([&]
	{
		std::for_each(std::begin(files), std::end(files), [&](const std::string& filePath) {
			CFileParser::PARSE_RESULT res;
			ParseFileAndOutput(filePath, cmdParser.GetSearchString());
		});
	});
	_tprintf(_T("Serial search time: %f milliseconds.\n"), elapsed);
	_tprintf(_T("\n"));

	// paralel for each
	_tprintf(_T("START Parallel search\n"));
	elapsed = time_call([&]
	{
		parallel_for_each(std::begin(files), std::end(files), [&](const std::string& filePath) {
			ParseFileAndOutput(filePath, cmdParser.GetSearchString());
		});
	});
	_tprintf(_T("Paralel search time: %f milliseconds.\n"), elapsed);
	_tprintf(_T("\n"));

	// thread for each
	_tprintf(_T("START Thread search\n"));
	elapsed = time_call([&]
	{
		CThreadFileParser parser(files, cmdParser.GetSearchString(),4);
		parser.InitInternal();
	});
	_tprintf(_T("Thread search time: %f milliseconds.\n"), elapsed);
	_tprintf(_T("\n"));

	getchar();

	return 0;
}

