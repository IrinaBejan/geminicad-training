#pragma once
struct SInfoFound
{
	SInfoFound();
	~SInfoFound();

	SInfoFound(const SInfoFound& source);
	SInfoFound& operator =  (const SInfoFound& source);
	bool        operator == (const SInfoFound& source);
	void Reset();

	int         m_fileOffset;
	std::string m_fileName;
	std::string m_prefix;
	std::string m_sufix;
};

class CFileParser
{
public:
	typedef  std::vector<SInfoFound> PARSE_RESULT;

	CFileParser(const std::string& filePath, const std::string& searchStr);
	virtual ~CFileParser();

	bool ParseFile(PARSE_RESULT& result);

private:
	void GetPrefixAndSuffix(const std::string& line, int pos, int textLen, std::string& prefix, std::string& suffix);

private:
	std::string    m_filePath;
	std::string    m_searchStr;
};



