#include "stdafx.h"
#include "ComandLineParser.h"
#include "Shlwapi.h"
using namespace std;
//!
//!........................................Constructor................................................
//! In : arguments from command line
//!
CComandLineParser::CComandLineParser(int size, _TCHAR** arguments)
{
	m_arguments = arguments;
	m_size = size;
}
//!
//!........................................Destructor.................................................
//!
CComandLineParser::~CComandLineParser()
{
	m_files.clear();
}
//!
//!........................................Getter for files paths vector..............................
//! Out: vector of files paths
//!
bool CComandLineParser::GetFilesPaths(FILE_PATHS_VEC& outVec)
{
	bool ok;

	outVec.clear();
	ok = ParseCommandLineArgv();
	ok = ok && m_files.size();
	
	if (ok)
		outVec = m_files;
	else
		_tprintf(TEXT(" Inpunt is not valid!!\n"));

	return ok;
}
//!
//!........................................Parse comand line and get the paths........................
//!
bool CComandLineParser::ParseCommandLineArgv()
{
	bool ok;
	
	ok =  (m_size >= 3);

	for (int i = 1; ok && i < m_size; i++)
	{
		string str = string(m_arguments[i]);
		if (i == 1)
		{
			if (PathIsDirectory(str.c_str()))
				CheckFolderContent(str);
			else
			{
				if (IsValidFile(str) == true)
					m_files.push_back(str);
			}
		}
		else
			m_searchStr += (i==2 ? str : std::string(_T(" ")) + str);
	}
	
	return ok;
}
//!
//!........................................Check if we have a valid file..............................
//!
bool CComandLineParser::IsValidFile(const string& filePath)
{
	bool valid;

	valid = (PathFileExists(filePath.c_str()) == 1);

	return valid;
}
//!
//!........................................Go thru all files in the directory and check them..........
//!
void CComandLineParser::CheckFolderContent(const string& folderPath)
{
	WIN32_FIND_DATA ffd;
	HANDLE hFile= INVALID_HANDLE_VALUE;
	
	// Find the first file in the directory to do this we need to have a path that ends with \\*
	char* strFolder = const_cast<char*>(folderPath.c_str());
	PathRemoveBackslash(strFolder);
	string newStr = string(strFolder) + _T("\\*");

	hFile = FindFirstFile(newStr.c_str(), &ffd);
	if (INVALID_HANDLE_VALUE == hFile)
		return ;

	// Find all the files in the folder
	do
	{

		if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			m_files.push_back(string(strFolder) + _T("\\") + string(ffd.cFileName));
		else
		{
			if (!(ffd.cFileName == std::string(_T(".")) || ffd.cFileName == std::string(_T(".."))))
			{
				std::string subFolder = string(strFolder) + _T("\\") + ffd.cFileName;
				if (PathIsDirectory(subFolder.c_str()))
					CheckFolderContent(string(strFolder) + _T("\\") + ffd.cFileName);
			}
		}

	} while (FindNextFile(hFile, &ffd) != 0);
	FindClose(hFile);
}
//!
//!........................................Return a const value of the search string..................
//!
const string& CComandLineParser::GetSearchString()
{
	return m_searchStr;
}