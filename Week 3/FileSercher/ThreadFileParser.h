#pragma once
#include <windows.h>

typedef  std::vector<std::pair<std::string,bool>> PARAMFILES;
typedef  std::vector<std::string> FILE_LIST;

struct ThreadParam
{
	PARAMFILES*  m_filePaths;
	std::string  m_stringToSearch;
	HANDLE       m_mutexHandler;
};

typedef  std::vector<HANDLE>       HANDLE_VEC;
typedef  std::vector<ThreadParam*> PARAM_VEC;

class CThreadFileParser
{
public:
	CThreadFileParser(const FILE_LIST& files, const std::string& searchStr, int threadNum);
	virtual ~CThreadFileParser();
    bool InitInternal();

static void ParseFile(void* lpParam);

private:
	int         m_threadNum;
	HANDLE_VEC  m_workThreads;
	HANDLE      m_mutex;
	PARAM_VEC   m_params;
	PARAMFILES  m_files;
	std::string m_searchStr;
};

