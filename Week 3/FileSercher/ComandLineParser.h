#pragma once
class CComandLineParser
{
public:
	typedef std::vector<std::string> FILE_PATHS_VEC;
	
	CComandLineParser(int size, _TCHAR** arguments);
	virtual ~CComandLineParser();

	bool GetFilesPaths(FILE_PATHS_VEC& outVec);
	const std::string& GetSearchString();

private:
	bool ParseCommandLineArgv();
	bool IsValidFile(const std::string& filePath);
	
	void CheckFolderContent(const std::string& folderPath);

private:
	FILE_PATHS_VEC   m_files;
	_TCHAR**         m_arguments;
	int              m_size;
	std::string      m_searchStr;
};

