#include "include\ContactsList.h"

ContactsList* ContactsList::m_instance = 0;

ContactsList* ContactsList::Instance()
{
  if (m_instance == nullptr)
  {
    m_instance = new ContactsList();
  }
  
  return m_instance;
}

ContactsList::ContactsList()
{
  contacts = vector<Contact*>();
}

void ContactsList::AddContact(Contact* target)
{
  contacts.push_back(target);
}


void ContactsList::RemoveContact()
{
	contacts.pop_back();
}

vector<Contact*>::const_iterator ContactsList::begin()
{
  return contacts.cbegin();
}

vector<Contact*>::const_iterator ContactsList::end()
{
  return contacts.cend();
}
