#include "include\CommandManager.h"

#include <stdio.h>

CommandManager* CommandManager::m_instance = 0;

CommandManager* CommandManager::Instance()
{
  if (m_instance == nullptr)
  {
    m_instance = new CommandManager();
  }
  
  return m_instance;
}

CommandManager::CommandManager()
{
  m_commands = map<std::string, ICommand*>();
}

void CommandManager::PrintUsage()
{
  for (auto pair : m_commands)
  {
    printf("%s - %s\n", pair.first.c_str(), pair.second->Usage().c_str());
  }
}

bool CommandManager::RegisterCommand(ICommand* target)
{
  try 
  {
    m_commands.insert_or_assign(target->Name(), target);
  }
  catch(const exception&)
  {
    return false;
  }
  
  return true;
}

CommandStatus CommandManager::ProcessArgV(int argumentCount, char* argumentValues[])
{
  int i = 1; //0 is the command line executable used and its path
  
  CommandStatus result;
  
  while (i < argumentCount)
  {
	  std::string commandName(argumentValues[i++]); //skip past the command name
    
    auto currentCommand = m_commands.find(commandName);
    
    if (currentCommand != m_commands.end())
    {
      result.Combine(
            currentCommand->second->ExecuteWithArguments(i, argumentCount, argumentValues)
            );
    }
  }
  
  return result;
}
