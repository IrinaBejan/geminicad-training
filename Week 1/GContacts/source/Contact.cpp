#include "include\Contact.h"

const char Contact::formatString[] = "%s, %s - %s\n";

Contact::Contact()
{
  
}

Contact::~Contact()
{
  
}

std::string Contact::ToString() const
{
	char buffer[1024];
	sprintf(buffer, formatString, lastName.c_str(), firstName.c_str(), phoneNumber.c_str());
	return std::string(buffer);
  //return std::string(formatString).arg(lastName, firstName, phoneNumber);
}
