#include "include\RemoveCommand.h"

#include <stdio.h>

#include "include\Contact.h"
#include "include\ContactsList.h"

#include "include\CommandManager.h"

RemoveCommand removeCommand;

RemoveCommand::RemoveCommand()
{
	CommandManager::Instance()->RegisterCommand(this);
}

RemoveCommand::~RemoveCommand()
{

}

std::string RemoveCommand::Name() const
{
	return std::string("remove");
}

std::string RemoveCommand::Usage() const
{
	return std::string("GContacts removed last contact");
}

CommandStatus RemoveCommand::ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[])
{
	printf("Processing REMOVE request.\n");
	ContactsList::Instance()->RemoveContact();
	printf("Contact removed.\n\n");

	return CommandStatus(CommandStatus::eCommandStatus::success, "");
}
