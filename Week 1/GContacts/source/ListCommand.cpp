#include "include\ListCommand.h"

#include "include\Contact.h"
#include "include\ContactsList.h"

#include "include\CommandManager.h"

ListCommand listCommand;

ListCommand::ListCommand()
{
    CommandManager::Instance()->RegisterCommand(this);
}

ListCommand::~ListCommand()
{
  
}

std::string ListCommand::Name() const
{
  return std::string("list");
}

std::string ListCommand::Usage() const
{
  return std::string("GContacts list");
}

CommandStatus ListCommand::ExecuteWithArguments(int&, int, char*[])
{
  printf("Contacts:\n");
  for (auto contact : *ContactsList::Instance())
  {
    printf("\t%s\n", contact->ToString().c_str());
  }
  
  printf("\n");
  
  return CommandStatus(CommandStatus::eCommandStatus::success, "");  
}
