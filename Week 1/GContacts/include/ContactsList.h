#pragma once

#include <vector>
#include "include\Contact.h"

using namespace std;

///
/// \brief Used to store and manage contact entries.
///
class ContactsList
{
public:
  static ContactsList* Instance();
  
  void AddContact(Contact* target);
  void RemoveContact();
  vector<Contact*>::const_iterator begin();
  vector<Contact*>::const_iterator end();

private:
  ContactsList();  
  
private: /*fields*/
  static ContactsList* m_instance;
  vector<Contact*> contacts;
};
