#pragma once

#include "include\ICommand.h"

///
/// \brief Command used to add contacts to the ContactsList
///
class AddCommand : public ICommand
{
public:
  AddCommand();
  virtual ~AddCommand();

public: /*ICommand*/
  virtual std::string Name() const override;
  virtual std::string Usage() const override;
  virtual CommandStatus ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[]) override;
};
