#pragma once

//class QString; //forward declare instead of include

#include <string>

///
/// \brief Interface allowing an object to specify its own string representation.
///
class IStringRepresentable
{
public:
  virtual std::string ToString() const = 0;
};
