#pragma once

#include "include\ICommand.h"

///
/// \brief Command used to list all the Contacts in currently in the ContactsList
///
class ListCommand : public ICommand
{
public:
  ListCommand();
  virtual ~ListCommand();

public: /*ICommand*/
  virtual std::string Name() const override;
  virtual std::string Usage() const override;
  virtual CommandStatus ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[]) override;  
};
