#pragma once

#include <map>

#include "include\Contact.h"

using namespace std;

///
/// \brief Decorator allowing the addition of unstructured fields to contacts.
///
class ExtendedContact : public Contact, public map<std::string, std::string>
{
public:
  ExtendedContact();
  virtual ~ExtendedContact();
  
protected:
  static const char exFormatString[];
  
public: /*IStringRepresentable*/
  virtual std::string ToString() const override;
  
public:
	std::string SafeGetField(std::string fieldName);
  void SetField(std::string fieldName, std::string fieldValue);
};
