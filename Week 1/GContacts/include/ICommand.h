#pragma once
#include <string>

///
/// \brief Structure used to communicate the end state of a run command.
///
struct CommandStatus
{
  enum eCommandStatus
  {
    success = 0,
    failure = 1,
    error   = 2,
  } result;
  
  std::string message;
  
  CommandStatus()
  {
    
  }
  
  CommandStatus(eCommandStatus _result, std::string _message)
  {
    result = _result;
    message = _message;
  }

  void Combine(const CommandStatus &other)
  {
    result = other.result > result ? other.result : result;
	char buffer[1024];
	sprintf(buffer, "%s\n%s", message.c_str(), other.message.c_str());
	message = std::string(buffer);
    //message = std::string("%1\n%2").arg(message, other.message);
  }
};

///
/// \brief Interface for all commands.
///
class ICommand
{
public:
  ///
  /// \brief The command's name. This will be used to invoke it. Case sensitive.
  /// \return 
  ///
  virtual std::string Name() const = 0;
  
  ///
  /// \brief The command's usage hint and any other helpful information about its use.
  /// \return 
  ///
  virtual std::string Usage() const = 0;
  
  virtual CommandStatus ExecuteWithArguments(int& currentArgument, int argumentCount, char* argumentValues[]) = 0;
};
