#pragma once

#include <numeric>
#include <vector>
#include <string>

typedef std::vector<uint8_t> DataStream;
///
/// \brief Class that provides base functionality to serialize and deserialize objects (with focus only on string and vector<T>)
///
template <class T>
class Serializer
{
public:
	Serializer();
	~Serializer();

	//size_t ComputeSize(const T& obj);
	size_t ComputeSize(const std::vector<T> &obj);
	size_t ComputeSize(const std::string &obj);
	void RecSerialize(const std::string& obj, DataStream::iterator& res);
	void RecSerialize(const std::vector<T>& obj, DataStream::iterator& res);
	void InitiateSerialization(const T& obj, DataStream& res);
	T InitiateDeserialization(DataStream::const_iterator & begin, const DataStream::const_iterator & end);
	void SerializeObject(const T& obj, DataStream::iterator& res);

};

template <class  T>
Serializer<T>::Serializer()
{
}

template <class  T>
Serializer<T>::~Serializer()
{
}

//
//template <class T>
//size_t Serializer<T>::ComputeSize(const T& obj)
//{
//	return size_t();
//}

template <class T>
size_t Serializer<T>::ComputeSize(const std::vector<T>& obj)
{
	return std::accumulate( obj.begin(), obj.end(), sizeof(size_t),
							[](const size_t& acc, const T& cur)
							{
								return acc + ComputeSize(cur); //for nested structures
							});
}

template <class T>
size_t Serializer<T>::ComputeSize(const std::string& obj)
{
	return sizeof(size_t) + obj.length() * sizeof(uint8_t);
}


template <class T>
void Serializer<T>::RecSerialize(const std::string& obj, DataStream::iterator& res)
{
	SerializeObject(obj.length(), res);
	for (const auto& cur : obj)
	{
		SerializeObject(obj.length(), res);
	}
}

template <class T>
void Serializer<T>::RecSerialize(const std::vector<T>& obj, DataStream::iterator& res)
{
	SerializeObject(obj.size(), res);
	for (const auto& cur : obj)
	{
		SerializeObject(cur, res);
	}
}

template <class T>
void Serializer<T>::SerializeObject(const T& obj, DataStream::iterator& res)
{
	RecSerialize(obj, res);
}

template <class T>
void Serializer<T>::InitiateSerialization(const T& obj, DataStream& res)
{
	size_t offset = res.size();
	size_t size = ComputeSize(obj);
	res.resize(res.size() + size);

	auto it = res.begin() + offset;
	SerializeObject(obj, it);
//	assert(res.begin() + offset + size == it);
}


template <class T>
std::string Serializer<string>::Deserialize(DataStream::const_iterator& begin, DataStream::const_iterator end)
{
	size_t size = Deserialize<size_t>(begin, end);

	if (size == 0u) return std::string();

	std::string str(size, '\0');
	for (size_t i = 0; i<size; ++i) {
		str.at(i) = Deserialize<uint8_t>(begin, end);
	}
	return str;
}


template <class T>
T Serializer<T>::InitiateDeserialization(DataStream::const_iterator& begin, const DataStream::const_iterator& end) 
{
	return Deserialize<T>(begin, end);
}
