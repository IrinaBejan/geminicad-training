QT += core
QT -= gui

CONFIG += c++11

TARGET = GContacts
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    source/CommandManager.cpp \
    source/AddCommand.cpp \
    source/Contact.cpp \
    source/ListCommand.cpp \
    source/ContactsList.cpp \
    source/ExtendedContact.cpp

HEADERS += \
    include/CommandManager.h \
    include/ICommand.h \
    include/AddCommand.h \
    include/IStringRepresentable.h \
    include/Contact.h \
    include/ListCommand.h \
    include/ContactsList.h \
    include/ExtendedContact.h
