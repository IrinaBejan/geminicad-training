#include <stdio.h>

#include "include\CommandManager.h"
#include "include\AddCommand.h"
#include "include\ListCommand.h"
#include "include\RemoveCommand.h"
#include "Serializer.h"


const char usageString[] = "usage: GContacts command [arguments].\nType 'GContacts help command_name' to find out more about a specific command.\n";

void PrintUsage()
{
  printf (usageString);
  
  CommandManager::Instance()->PrintUsage();
}

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    PrintUsage();
  }
  
  CommandStatus result = CommandManager::Instance()->ProcessArgV(argc, argv);
  
  if (result.result == CommandStatus::error)
  {
    perror(result.message.c_str());
  }
  else if (result.result == CommandStatus::failure)
  {
    printf("Failed: %s\n", result.message.c_str());
  }
/*  try
  {
	  Serializer<string> sel;
	  string q = "ana";
	  DataStream data;
	  sel.InitiateSerialization(q,data);
	  printf("data is %s", data);
  }
  catch (exception const& ex)
  {
	  string a = a + "Exception: ";
	  a += ex.what();
	  printf("ERROR: %s\n", a.c_str());
  }*/
  return result.result == CommandStatus::success;
}
